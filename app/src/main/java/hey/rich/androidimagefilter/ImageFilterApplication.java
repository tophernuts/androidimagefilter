package hey.rich.androidimagefilter;

import android.app.Application;
import android.content.Context;

/**
 * Created by chris on 22/01/15.
 */
public class ImageFilterApplication extends Application {

    private static Context mContext;

    @Override
    public void onCreate(){
        super.onCreate();
        mContext = getApplicationContext();
    }

    public static Context getContext(){
        return mContext;
    }
}
