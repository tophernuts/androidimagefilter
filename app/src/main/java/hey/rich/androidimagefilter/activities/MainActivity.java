package hey.rich.androidimagefilter.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hey.rich.androidimagefilter.R;
import hey.rich.androidimagefilter.adapters.ImageAdapter;
import hey.rich.androidimagefilter.utilities.FileUtils;


public class MainActivity extends Activity {

    private final static String TAG = Activity.class.getName();

    private static final int PICTURE_FROM_CAMERA = 0;
    private static final int PICTURE_FROM_GALLERY = 1;

    public static final String IMAGE_FILTER_LOCATION = "ImageFilter/";

    private List<String> items = new ArrayList<String>();
    /**
     * Dirty hack since I can't pass the Location in the Intent:
     * http://stackoverflow.com/a/12564910/1684866
     */
    private Uri pictureLocationUri = null;

    private ImageAdapter imageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GridView gridView = (GridView) findViewById(R.id.gridView);
        gridView.setEmptyView(findViewById(R.id.empty_grid_view));

        imageAdapter = new ImageAdapter(this, R.layout.grid_image, items);

        gridView.setAdapter(imageAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), PhotoEditorActivity.class);
                i.putExtra(PhotoEditorActivity.INTENT_EXTRA_FILE_NAME, items.get(position));
                startActivity(i);
            }
        });
    }

    private void populateList() {
        if (!FileUtils.isExternalStorageReadable()) {
            // We can't read shit
            return;
        }
        // TODO: Don't hardcode shit
        File f = FileUtils.getAlbumStorageDir(IMAGE_FILTER_LOCATION);

        // TODO: better check for files
        if (f == null) {
            // No directory == no pictures
            return;
        }

        File[] pictures = f.listFiles();
        if (pictures == null) {
            // Again no pics
            return;
        }

        items.clear();

        for (File pic : pictures) {
            // TODO: Have a better check if file is a picture
            if (pic.getAbsolutePath().contains(".png") || pic.getAbsolutePath().contains(".jpg")) {
                items.add(pic.getAbsolutePath());
            }
        }
        if (imageAdapter != null) {
            imageAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        populateList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Intent intent;
        switch (id) {
            case R.id.action_settings:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_help:
                intent = new Intent(this, HelpActivity.class);
                startActivity(intent);
                return true;
            case R.id.new_picture:
                openImageIntent();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openImageIntent() {
        final CharSequence[] options = {
                "Take Photo", "Choose from Gallery", "Cancel"
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        // TODO: No hardcode strings pls
        builder.setTitle("Add new photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                Intent intent;
                // Clear the pictureLocationUri
                pictureLocationUri = null;
                switch (item) {
                    case 0:
                        intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
                        String pictureName = "image-" + simpleDateFormat.format(new Date()) + ".jpg";

                        File f = new File(FileUtils.getAlbumStorageDir(IMAGE_FILTER_LOCATION), pictureName);
                        pictureLocationUri = Uri.fromFile(f);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, pictureLocationUri);

                        startActivityForResult(intent, PICTURE_FROM_CAMERA);
                        break;
                    case 1:
                        intent = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, PICTURE_FROM_GALLERY);
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                    default:
                        // Would be pretty impressive if we hit this
                        break;
                }
            }
        });
        builder.show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        Intent intent = new Intent(this, PhotoEditorActivity.class);

        // TODO: Maybe do this a little nicer
        if (resultCode != RESULT_OK) return;

        /*
         What a hack: http://stackoverflow.com/a/12564910/1684866
         Turns out that when taking the picture, the URI that I used to set
         the file of the picture will be in a different format from just loading
         an image from the gallery.
         This converts the URI to the correct version.
         */
        if (imageReturnedIntent == null) {
            try {
                pictureLocationUri = Uri.parse(
                        MediaStore.Images.Media.insertImage(
                                getContentResolver(),
                                pictureLocationUri.getPath(), null, null
                        )
                );
            } catch (FileNotFoundException e) {
                // Fucking really?
                Toast.makeText(this, "Error opening photo", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        // TODO: Oh god this is so disgusting
        if (pictureLocationUri == null) {
            pictureLocationUri = imageReturnedIntent.getData();
        }

        // TODO: This is so disgusting
        switch (requestCode) {
            case PICTURE_FROM_CAMERA:
            case PICTURE_FROM_GALLERY:
                intent.putExtra(PhotoEditorActivity.INTENT_EXTRA_FILE_NAME,
                        FileUtils.getRealPathFromURI(this, pictureLocationUri));
                break;
            default:
                // What in the fuck
                return;
        }
        startActivity(intent);
    }

}
