package hey.rich.androidimagefilter.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import hey.rich.androidimagefilter.R;
import hey.rich.androidimagefilter.filters.ImageFilter;
import hey.rich.androidimagefilter.filters.MeanFilter;
import hey.rich.androidimagefilter.filters.MedianFilter;
import hey.rich.androidimagefilter.listeners.ImageGestureDetector;
import hey.rich.androidimagefilter.tasks.FilterTask;
import hey.rich.androidimagefilter.tasks.WarpTask;
import hey.rich.androidimagefilter.utilities.FileUtils;
import hey.rich.androidimagefilter.utilities.ImageBackup;
import hey.rich.androidimagefilter.utilities.ImageLoader;
import hey.rich.androidimagefilter.warps.FishEye;
import hey.rich.androidimagefilter.warps.Kaleidoscope;
import hey.rich.androidimagefilter.warps.Ripple;

public class PhotoEditorActivity extends Activity implements FilterTask.FilterTaskCompleteListener, ImageGestureDetector.GestureCallback {

    public static final String INTENT_EXTRA_FILE_NAME = "picture_here";

    private static final String TAG = PhotoEditorActivity.class.getSimpleName();

    /**
     * Represents current filterMode
     */
    private int filterTypeValue = 0;
    /**
     * Represents current filterSize
     */
    private int filterSizeValue = 1;

    private FilterTask filterTask;

    /**
     * fileName of image to be loaded
     */
    private String fileNameString;

    private Bitmap bitmap = null;

    private ImageView imageView;

    private ImageBackup<Uri> imageBackup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_editor);

        Intent i = getIntent();

        // TODO: This is so sloppy, have everyone just pass in URI as data and then get the
        // String out myself here instead of doing that in MainActivity

        fileNameString = i.getStringExtra(INTENT_EXTRA_FILE_NAME);
        // TODO: Look into shit where onCreate is called again, but w/o extras
        if (fileNameString == null) {
            // Shit we didn't get a file
            //Toast.makeText(this, "Error opening picture", Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Error opening picture: no fileName in intent");

            fileNameString = FileUtils.getRealPathFromURI(this, i.getData());
            if (fileNameString == null) {
                finish();
            }
        }

        imageView = (ImageView) findViewById(R.id.image_editor);
        setUpUIElements();
        final GestureDetector gestureDetector = new GestureDetector(new ImageGestureDetector(PhotoEditorActivity.this));
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                // TODO: Should this be dependent on returnValue of gestureDetector.onTouchEvent?
                return true;
            }
        });
    }

    public void filterCompleted(Bitmap result) {
        // TODO: Remove all this shit, just have the loadPicture()
        if (result.sameAs(this.bitmap)) {
            // Nothing changed what the fuck, hmm that could be good though
            loadPicture();
        } else {
            this.bitmap = result;
            loadPicture();
        }
    }

    public void warpCompleted(Bitmap result) {
        this.bitmap = result;
        loadPicture();
    }

    private void loadPicture() {
        // TODO: Should we really be getting new image every time?

        if (bitmap == null) {
            ImageLoader imageLoader = new ImageLoader(R.id.image_editor);
            imageLoader.loadBitmap(fileNameString, imageView);
            this.bitmap = BitmapFactory.decodeFile(fileNameString);
        } else {
            imageView.setImageBitmap(bitmap);
        }
    }

    /**
     * Loads default values from settings
     */
    private void loadDefaults() {
        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            filterTypeValue = Integer.valueOf(sharedPreferences.getString("default_filter", "0"));

            if (filterTypeValue == 0) {
                filterSizeValue = Integer.valueOf(
                        sharedPreferences.getString("mean_filter_size", "0"));
            } else {
                filterSizeValue = Integer.valueOf(
                        sharedPreferences.getString("median_filter_size", "0"));
            }


            // Set up ImageBackup
            imageBackup = new ImageBackup<Uri>(PhotoEditorActivity.this, Integer.valueOf(
                    sharedPreferences.getString("undo_size", "0")));
        } catch (NumberFormatException e) {
            Log.e(TAG, "Error loading default values");
        }
    }

    public void swipeRight() {
        WarpTask warpTask = new WarpTask(PhotoEditorActivity.this, this.imageBackup);
        warpTask.execute(new Ripple(PhotoEditorActivity.this, PhotoEditorActivity.this.bitmap));
    }

    public void swipeLeft() {
        WarpTask warpTask = new WarpTask(PhotoEditorActivity.this, this.imageBackup);
        warpTask.execute(new FishEye(PhotoEditorActivity.this, PhotoEditorActivity.this.bitmap));
    }

    public void swipeVertical() {
        WarpTask warpTask = new WarpTask(PhotoEditorActivity.this, this.imageBackup);
        warpTask.execute(new Kaleidoscope(PhotoEditorActivity.this, PhotoEditorActivity.this.bitmap));
    }

    private void setUpUIElements() {
        loadPicture();

        loadDefaults();

        final NumberPicker filterType = (NumberPicker) findViewById(R.id.filter_type);
        NumberPicker filterSize = (NumberPicker) findViewById(R.id.filter_size_picker);

        // TODO: Don't hardcode this shit
        // TODO: Get set/default value from settings
        filterType.setDisplayedValues(new String[]{"Median", "Mean"});
        filterType.setMinValue(0);
        filterType.setMaxValue(1);
        filterSize.setDisplayedValues(getResources().getStringArray(R.array.pref_mean_filter_list));
        filterSize.setMinValue(0);
        filterSize.setMaxValue(24);

        filterType.setValue(filterTypeValue);
        filterSize.setValue(filterSizeValue);

        filterType.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                filterTypeValue = newVal;
            }
        });

        filterSize.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                filterSizeValue = newVal;
            }
        });

        ImageButton imageButton = (ImageButton) findViewById(R.id.edit_button);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: Implement actual filtering here
                ImageFilter filter;
                if (filterTypeValue == 0) {
                    // Median
                    filter = new MedianFilter(
                            PhotoEditorActivity.this.bitmap, new Integer(
                            getResources().getStringArray(R.array.pref_mean_filter_list)[filterSizeValue]));
                } else {
                    // Mean
                    filter = new MeanFilter(
                            PhotoEditorActivity.this.bitmap, new Integer(
                            getResources().getStringArray(R.array.pref_mean_filter_list)[filterSizeValue]));
                }
                filterTask = new FilterTask(PhotoEditorActivity.this, filter, PhotoEditorActivity.this,
                        PhotoEditorActivity.this.imageBackup);
                filterTask.execute(filter);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.menu_photo_editor_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_save_changes:
                // Actually wait, no matter what we'll just save a new image here
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
                String pictureName = "image-" + simpleDateFormat.format(new Date()) + ".jpg";
                File f = new File(FileUtils.getAlbumStorageDir(MainActivity.IMAGE_FILTER_LOCATION), pictureName);
                FileUtils.saveBitmapToFile(f.getAbsolutePath(), bitmap);
                finish();
                return true;
            case R.id.menu_cancel_changes:
                // TODO: If we just created this picture do we delete it?
                // I say no since I'm lazy
                finish();
                return true;
            case R.id.menu_undo_changes:
                if (!imageBackup.empty()) {
                    Uri tempInCaseWeGetNullBack = imageBackup.pop();

                    if ("file".equalsIgnoreCase(tempInCaseWeGetNullBack.getScheme())) {
                        String fileName = tempInCaseWeGetNullBack.getPath();
                        ImageLoader imageLoader = new ImageLoader(R.id.image_editor);
                        imageLoader.loadBitmap(fileName, imageView);
                        this.bitmap = BitmapFactory.decodeFile(fileName);
                        loadPicture();
                    } else {
                        Log.e(TAG, "Error recovering picture");
                    }
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy() {
        if (filterTask != null && filterTask.getStatus() != AsyncTask.Status.FINISHED) {
            filterTask.cancel(true);
        }
        super.onDestroy();
    }


}
