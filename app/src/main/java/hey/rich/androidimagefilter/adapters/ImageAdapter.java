package hey.rich.androidimagefilter.adapters;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import hey.rich.androidimagefilter.R;
import hey.rich.androidimagefilter.utilities.ImageLoader;

/**
 * Created by chris on 22/01/15.
 * From: http://developer.android.com/guide/topics/ui/layout/gridview.html
 *
 * TODO: If this is still being a bitch, check out:
 * https://github.com/nostra13/Android-Universal-Image-Loader
 */
public class ImageAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> items = new ArrayList<String>();
    private ImageLoader imageLoader;

    private int imageWidth;


    public ImageAdapter(Context c, int resource, List<String> objects){
        super(c, resource, objects);
        this.context = c;
        this.items = objects;


        imageLoader = new ImageLoader(R.drawable.ic_launcher);
        // TODO: This should be done magically via the xml
        int screenWidth = getScreenWidth();
        imageWidth = screenWidth/2 - screenWidth/20 ;
    }

    private int getScreenWidth(){
        DisplayMetrics metrics;
        metrics = context.getResources().getDisplayMetrics();
        return metrics.widthPixels;
    }

    public int getCount(){
        return items.size();
    }

    public String getItem(int position){
        return items.get(position);
    }

    // Create a new ImageView for each item referenced by the adapter
    public View getView(int position, View convertView, ViewGroup parent){
        ImageView imageView;
        if(convertView == null){
            imageView = new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(imageWidth, imageWidth));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }else{
            imageView = (ImageView) convertView;
        }

        imageLoader.loadBitmap(getItem(position), imageView);
        return imageView;
    }


}
