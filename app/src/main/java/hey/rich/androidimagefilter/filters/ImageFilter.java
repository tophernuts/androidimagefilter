package hey.rich.androidimagefilter.filters;

import android.graphics.Bitmap;

/**
 * Created by chris on 24/01/15.
 */
public abstract class ImageFilter {

    /** Minimum size of the filter max*/
    public static final int MIN_SIZE = 3;

    protected Bitmap bitmap;
    protected int size;
    public boolean cancel;

    public ImageFilter(Bitmap bitmap, int size){
        this.bitmap = bitmap;

        // Don't let mask size > bitmap
        int maxSize = Math.min(bitmap.getWidth(), bitmap.getHeight());
        if(size < MIN_SIZE){
            this.size = MIN_SIZE;
        }else if(size > maxSize){
            this.size = maxSize;
        }else{
            this.size = size;
        }
        this.cancel = false;
    }

    public Bitmap getBitmap(){
        return this.bitmap;
    }

    /** Applies the filter */
    public abstract Bitmap apply();
}
