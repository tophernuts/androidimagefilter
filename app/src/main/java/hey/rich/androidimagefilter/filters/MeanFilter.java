package hey.rich.androidimagefilter.filters;

import android.graphics.Bitmap;
import android.graphics.Color;

public class MeanFilter extends ImageFilter {

    public MeanFilter(Bitmap image, int size) {
        super(image, size);
    }

    @Override
    public Bitmap apply() {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int offset = size / 2;

        int[] pixels = new int[width * height];
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);

        // Get all the new pixels and calculate their new values
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                // Check if some silly folk has cancelled me
                if (cancel) return null;

                int r = 0;
                int g = 0;
                int b = 0;
                int a = 0;

                // Get the new mean value for each new pixel
                int maskPixels = 0;
                for (int row = y - offset; row <= y + offset; row++) {
                    for (int col = x - offset; col <= x + offset; col++) {
                        if (row >= 0 && col >= 0 && row < height && col < width) {
                            int color = pixels[row * width + col];

                            r += Color.red(color);
                            g += Color.green(color);
                            b += Color.blue(color);
                            a += Color.alpha(color);

                            maskPixels++;
                        }
                    }
                }
                r = r / maskPixels;
                g = g / maskPixels;
                b = b / maskPixels;
                a = a / maskPixels;

                pixels[y * width + x] = Color.argb(a, r, g, b);
            }
        }

        return Bitmap.createBitmap(pixels, width, height, bitmap.getConfig());
    }
}
