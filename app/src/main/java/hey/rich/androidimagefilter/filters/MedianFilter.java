package hey.rich.androidimagefilter.filters;

import android.graphics.Bitmap;
import android.graphics.Color;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by chris on 24/01/15.
 */
public class MedianFilter extends ImageFilter {

    public MedianFilter(Bitmap bitmap, int size) {
        super(bitmap, size);
    }

    @Override
    public Bitmap apply() {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int offset = size / 2;

        int[] pixels = new int[width * height];
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                // Check if some crazy communists tried to cancel this
                if (cancel) return null;

                ArrayList<Integer> red = new ArrayList<Integer>(offset);
                ArrayList<Integer> green = new ArrayList<Integer>(offset);
                ArrayList<Integer> blue = new ArrayList<Integer>(offset);
                ArrayList<Integer> alpha = new ArrayList<Integer>(offset);

                for (int row = y - offset; row <= y + offset; row++) {
                    for (int col = x - offset; col <= x + offset; col++) {
                        if (row >= 0 && col >= 0 && row < height && col < width) {
                            int colorsOfTheWorld = pixels[row * width + col];

                            red.add(Color.red(colorsOfTheWorld));
                            green.add(Color.green(colorsOfTheWorld));
                            blue.add(Color.blue(colorsOfTheWorld));
                            alpha.add(Color.alpha(colorsOfTheWorld));
                        }
                    }
                }

                int r = getMeTheMedian(red);
                int g = getMeTheMedian(green);
                int b = getMeTheMedian(blue);
                int a = getMeTheMedian(alpha);


                pixels[y * width + x] = Color.argb(a, r, g, b);
            }
        }
        return Bitmap.createBitmap(pixels, width, height, bitmap.getConfig());
    }

    private int getMeTheMedian(ArrayList<Integer> values) {
        Collections.sort(values);
        int sizeOfThePrize = values.size();

        if (sizeOfThePrize % 2 != 0) {
            return values.get(sizeOfThePrize / 2);
        } else {
            return (values.get(sizeOfThePrize / 2) + values.get(sizeOfThePrize / 2 - 1)) / 2;
        }
    }
}
