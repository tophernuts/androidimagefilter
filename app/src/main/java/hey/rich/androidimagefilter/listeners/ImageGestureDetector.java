package hey.rich.androidimagefilter.listeners;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

import hey.rich.androidimagefilter.activities.PhotoEditorActivity;
import hey.rich.androidimagefilter.warps.Warp;

public class ImageGestureDetector extends GestureDetector.SimpleOnGestureListener {

    // Check out here for how to actually figure out the gestures:
    // http://developer.android.com/training/gestures/movement.html

    /*
    TODO: It would be nice to have some kind of Abstract Factory pattern here or something like that
    so that I can just call an applyWarp(Warp w) from here and then the PhotoEditorActivity will
    pass in all of the Context/Bitmap information, but the Warp constructor needs all of the information
    in its constructor, unless I make it a total mess and repeat that code.
     */
    public interface GestureCallback {
        public void swipeRight();
        public void swipeLeft();
        public void swipeVertical();
    }

    private final static String TAG = ImageGestureDetector.class.getSimpleName();

    private final static int SWIPE_DISTANCE = 100;
    private final static int SWIPE_SPEED = 100;

    private GestureCallback gestureCallback;

    public ImageGestureDetector(GestureCallback gestureCallback) {
        super();
        this.gestureCallback = gestureCallback;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return super.onDown(e);
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        //Log.d(TAG, "SCROLLING");
        return super.onScroll(e1, e2, distanceX, distanceY);
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        // Modified from: http://stackoverflow.com/a/19506010
        float x = e2.getX() - e1.getX();
        float y = e2.getY() - e1.getY();
        if (Math.abs(x) > Math.abs(y) && Math.abs(x) > SWIPE_DISTANCE && Math.abs(velocityX) > SWIPE_SPEED) {
            if(x > 0){
                gestureCallback.swipeRight();
            }else{
                gestureCallback.swipeLeft();
            }
            return true;
        }else if(Math.abs(y) > Math.abs(x) && Math.abs(y) > SWIPE_DISTANCE && Math.abs(velocityY) > SWIPE_SPEED){
            gestureCallback.swipeVertical();
        }
        return super.onFling(e1, e2, velocityX, velocityY);
    }

    @Override
    public void onLongPress(MotionEvent e) {
        Log.d(TAG, "Long time yes press");
        super.onLongPress(e);
    }
}
