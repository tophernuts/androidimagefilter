package hey.rich.androidimagefilter.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import hey.rich.androidimagefilter.filters.ImageFilter;
import hey.rich.androidimagefilter.utilities.ImageBackup;

/**
 * This be a task to do some filtering you see mmkay
 */
public class FilterTask extends AsyncTask<ImageFilter, Void, Bitmap>{

    private ProgressDialog progressDialog;
    private ImageFilter filter;
    private FilterTaskCompleteListener filterActivity;
    private ImageBackup backup;

    /*
      TODO: This seems really silly that I ask for a filter in the Constructor
      and then again in the execute method
     */
    public FilterTask(Context context, ImageFilter filter, FilterTaskCompleteListener filterActivity,
                      ImageBackup backup){
        progressDialog = new ProgressDialog(context);
        this.filter = filter;
        this.filterActivity = filterActivity;
        this.backup = backup;
    }

    public interface FilterTaskCompleteListener{
        public void filterCompleted(Bitmap result);
    }

    @Override
    protected void onPreExecute(){
        progressDialog.setMessage("Filtering");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(true);

        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                filter.cancel = true;
                cancel(true);
            }
        });
        progressDialog.show();
    }

    @Override
    protected Bitmap doInBackground(ImageFilter... filters) {
        filter = filters[0];
        backup.pushBitmap(filter.getBitmap());
        return filter.apply();
    }

    @Override
    protected void onPostExecute(Bitmap result){
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        // Update the picture now
        filterActivity.filterCompleted(result);
    }

    @Override
    protected void onCancelled(Bitmap result){
        filter.cancel = true;
        onCancelled();
    }
}
