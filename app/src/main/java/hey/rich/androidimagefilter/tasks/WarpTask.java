package hey.rich.androidimagefilter.tasks;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import hey.rich.androidimagefilter.activities.PhotoEditorActivity;
import hey.rich.androidimagefilter.utilities.ImageBackup;
import hey.rich.androidimagefilter.warps.Warp;

/**
 * Created by chris on 07/02/15.
 */
public class WarpTask extends AsyncTask<Warp, Void, Bitmap> {

    private ProgressDialog dialog;
    private PhotoEditorActivity parentActivity;
    private Warp warp;
    private ImageBackup backup;

    public WarpTask(PhotoEditorActivity activity, ImageBackup backup){
        super();
        this.parentActivity = activity;
        this.dialog = new ProgressDialog(activity);
        this.backup = backup;
    }

    @Override
    protected void onPreExecute(){
        dialog.setMessage("Warping...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                cancel(true);
            }
        });
        dialog.show();
    }

    @Override
    protected Bitmap doInBackground(Warp... params){
        warp = params[0];

        backup.pushBitmap(warp.getBitmap());

        return warp.warp();
    }

    @Override
    protected void onPostExecute(Bitmap warpedBitmap){
        if(dialog.isShowing()){
            dialog.dismiss();
        }

        parentActivity.warpCompleted(warpedBitmap);
    }

    @Override
    protected void onCancelled(Bitmap result){
        warp.cancelWarp();
        onCancelled();
    }

}
