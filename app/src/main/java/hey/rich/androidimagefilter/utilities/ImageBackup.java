package hey.rich.androidimagefilter.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.util.Stack;

/**
 * Created by chris on 07/02/15.
 */
public class ImageBackup<T> extends Stack<T> {

    private Context context;
    private int size;

    public ImageBackup(Context context, int size) {
        super();
        this.context = context;
        this.size = size;
    }

    public T pushBitmap(Bitmap bitmap) {
        // If we too big we drop the old ones k
        if(this.size == 0) return null;
        while (this.size() >= this.size) {
            remove(0);
        }

        String tempFileName = String.format("%s/androidimagefilter-%d.png",
                context.getCacheDir().getAbsolutePath(), this.size());

        Log.d("EGGS", String.format("Pics in: %s", tempFileName));

        FileUtils.saveBitmapToFile(tempFileName, bitmap);
        return super.push((T) Uri.fromFile(new File(tempFileName)));
    }

    @Override
    public T remove(int location) {
        // If we're size 0, don't do nothing
        Uri fileUri = (Uri) super.get(location);
        if (fileUri != null) {
            File fileToRemove = new File(fileUri.getPath());
            if (fileToRemove.exists()) {
                fileToRemove.delete();
            }
        }
        return super.remove(location);
    }

    @Override
    public void clear() {
        while (this.size() > 0) {
            this.remove(0);
        }
    }
}
