package hey.rich.androidimagefilter.utilities;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

import hey.rich.androidimagefilter.ImageFilterApplication;
import hey.rich.androidimagefilter.R;

/**
 * Created by chris on 22/01/15.
 */
public class ImageLoader {

    private final String TAG = ImageLoader.class.getName();

    private Bitmap mPlaceHolderBitmap;
    private Context mContext;

    public ImageLoader(){
        this(R.drawable.ic_launcher);
    }

    public ImageLoader(int drawableId){
        mContext = ImageFilterApplication.getContext();
        mPlaceHolderBitmap = BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.ic_launcher);
    }


    // From: http://developer.android.com/training/displaying-bitmaps/display-bitmap.html
    // http://developer.android.com/training/displaying-bitmaps/process-bitmap.html#concurrency
    // http://developer.android.com/training/displaying-bitmaps/process-bitmap.html#BitmapWorkerTask
    // http://developer.android.com/training/displaying-bitmaps/load-bitmap.html#decodeSampledBitmapFromResource
    public void loadBitmap(int resId, ImageView imageView){
        if(cancelPotentialWork(resId, imageView)){
            final BitmapResourceWorkerTask task = new BitmapResourceWorkerTask(imageView);
            final AsyncDrawableResource asyncDrawableResource =
                    new AsyncDrawableResource(mContext.getResources(), mPlaceHolderBitmap, task);
            imageView.setImageDrawable(asyncDrawableResource);
            task.execute(resId);
        }
    }

    public void loadBitmap(String fileName, ImageView imageView){
        if(cancelPotentialWork(fileName, imageView)){
            final BitmapFileWorkerTask task = new BitmapFileWorkerTask(imageView);
            final AsyncDrawableFile asyncDrawableResource =
                    new AsyncDrawableFile(mContext.getResources(), mPlaceHolderBitmap, task);
            imageView.setImageDrawable(asyncDrawableResource);
            task.execute(fileName);
        }
    }

    // TODO: needs a better name, and to be combined with below
    static class AsyncDrawableResource extends BitmapDrawable {
        private final WeakReference<BitmapResourceWorkerTask> bitmapWorkerTaskReference;

        public AsyncDrawableResource(Resources res, Bitmap bitmap, BitmapResourceWorkerTask bitmapResourceWorkerTask){
            super(res, bitmap);
            bitmapWorkerTaskReference = new WeakReference<BitmapResourceWorkerTask>(bitmapResourceWorkerTask);
        }



        public BitmapResourceWorkerTask getBitmapResourceWorkerTask(){
            return bitmapWorkerTaskReference.get();
        }
    }

    static class AsyncDrawableFile extends BitmapDrawable{
        private final WeakReference<BitmapFileWorkerTask> bitmapFileWorkerTaskWeakReference;

        public AsyncDrawableFile(Resources res, Bitmap bitmap, BitmapFileWorkerTask bitmapFileWorkerTask){
            super(res, bitmap);
            bitmapFileWorkerTaskWeakReference = new WeakReference<BitmapFileWorkerTask>(bitmapFileWorkerTask);
        }

        public BitmapFileWorkerTask getBitmapFileWorkerTask(){
            return bitmapFileWorkerTaskWeakReference.get();
        }
    }

    public static boolean cancelPotentialWork(int data, ImageView imageView){
        final BitmapResourceWorkerTask bitmapResourceWorkerTask = getBitmapResourceWorkerTask(imageView);

        if(bitmapResourceWorkerTask != null){
            final int bitmapData = bitmapResourceWorkerTask.data;
            if(bitmapData != data){
                // Cancel previous task
                bitmapResourceWorkerTask.cancel(true);
            }else{
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with ImageView, or existing task cancelled
        return true;
    }

    public static boolean cancelPotentialWork(String fileName, ImageView imageView){
        final BitmapFileWorkerTask bitmapFileWorkerTask = getBitmapFileWorkerTask(imageView);

        if(bitmapFileWorkerTask != null){
            final String bitmapData = bitmapFileWorkerTask.data;
            if(!bitmapData.equals(fileName)){
                // Cancel previous task
                bitmapFileWorkerTask.cancel(true);
            }else{
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with ImageView, or existing task cancelled
        return true;
    }

    private static BitmapResourceWorkerTask getBitmapResourceWorkerTask(ImageView imageView){
        if (imageView != null){
            final Drawable drawable = imageView.getDrawable();
            if(drawable instanceof AsyncDrawableResource){
                final AsyncDrawableResource asyncDrawableResource = (AsyncDrawableResource) drawable;
                return asyncDrawableResource.getBitmapResourceWorkerTask();
            }
        }
        return null;
    }

    private static BitmapFileWorkerTask getBitmapFileWorkerTask(ImageView imageView){
        if(imageView != null){
            final Drawable drawable = imageView.getDrawable();
            if(drawable instanceof AsyncDrawableFile){
                final AsyncDrawableFile asyncDrawableFile = (AsyncDrawableFile) drawable;
                return asyncDrawableFile.getBitmapFileWorkerTask();
            }
        }
        return null;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight){
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static Bitmap decodeSampledBitmapFromFile(String fileName, int reqWidth, int reqHeight){
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileName, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(fileName, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth,
                                            int reqHeight){
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if(height > reqHeight || width > reqWidth){
            final int halfHeight = height/2;
            final int halfWidth = width /2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height/width
            while((halfHeight /inSampleSize) > reqHeight && (halfWidth/inSampleSize) > reqWidth){
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    private class BitmapFileWorkerTask extends AsyncTask<String, Void, Bitmap>{
        private final WeakReference<ImageView> imageViewReference;
        private String data = "";

        public BitmapFileWorkerTask(ImageView imageView){
            // Use a weak reference to ensure the ImageView can be garbage collected
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        // Decode image in background
        @Override
        protected  Bitmap doInBackground(String... params){
            data = params[0];
            // TODO: NO MAGIC NUMBERS
            return decodeSampledBitmapFromFile(data, 1000, 1000);
        }

        // Once complete, see if ImageView is still here and set its bitmap
        @Override
        protected void onPostExecute(Bitmap bitmap){
            if(bitmap != null){
                final ImageView imageView = imageViewReference.get();
                if(imageView != null){
                    imageView.setImageBitmap(bitmap);
                }
            }
        }
    }

    private class BitmapResourceWorkerTask extends AsyncTask<Integer, Void, Bitmap> {
        private final WeakReference<ImageView> imageViewReference;
        private int data = 0;

        public BitmapResourceWorkerTask(ImageView imageView){
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        // Decode image in background
        @Override
        protected Bitmap doInBackground(Integer... params){
            data = params[0];
            // TODO: No magic numbers
            return decodeSampledBitmapFromResource(mContext.getResources(), data, 100, 100);
        }

        // Once complete, see if ImageView is still around and set bitmap
        @Override
        protected void onPostExecute(Bitmap bitmap){
            if(bitmap != null){
                final ImageView imageView = imageViewReference.get();
                if(imageView != null){
                    imageView.setImageBitmap(bitmap);
                }
            }
        }
    }


}
