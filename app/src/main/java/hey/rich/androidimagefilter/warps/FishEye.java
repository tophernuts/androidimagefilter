package hey.rich.androidimagefilter.warps;

import android.content.Context;
import android.graphics.Bitmap;

/**
 * Created by chris on 07/02/15.
 */
public class FishEye extends Warp{

    public FishEye(Context context, Bitmap bitmap){
        super(context, bitmap);
    }

    @Override
    public Bitmap warp(){
        warpScript.invoke_fishEye();
        warpOutAllocation.copyTo(bitmap);
        return bitmap;
    }
}
