package hey.rich.androidimagefilter.warps;

import android.content.Context;
import android.graphics.Bitmap;

/**
 * Created by chris on 07/02/15.
 */
public class Kaleidoscope extends Warp{

    public Kaleidoscope(Context context, Bitmap bitmap){
        super(context, bitmap);
    }

    @Override
    public Bitmap warp(){
        warpScript.invoke_kaleidoscope();
        warpOutAllocation.copyTo(bitmap);
        return bitmap;
    }
}
