package hey.rich.androidimagefilter.warps;

import android.content.Context;
import android.graphics.Bitmap;

/**
 * Created by chris on 2/4/15.
 */
public class Ripple extends Warp{

    public Ripple(Context context, Bitmap bitmap){
        super(context, bitmap);
    }

    @Override
    public Bitmap warp(){
        warpScript.invoke_ripple();
        warpOutAllocation.copyTo(bitmap);
        return bitmap;
    }
}
