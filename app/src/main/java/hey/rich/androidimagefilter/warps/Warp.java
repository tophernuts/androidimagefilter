package hey.rich.androidimagefilter.warps;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.RenderScript;

import hey.rich.androidimagefilter.ScriptC_warp;

public abstract class Warp {

    protected Bitmap bitmap;
    protected ScriptC_warp warpScript;
    protected Allocation warpOutAllocation;

    public Warp(Context context, Bitmap bitmap) {
        this.bitmap = bitmap;

        // RENDER the SCRIPT
        RenderScript renderScript = RenderScript.create(context);

        Allocation warpInAllocation = Allocation.createFromBitmap(renderScript, bitmap);
        warpInAllocation.copyFrom(bitmap);

        warpOutAllocation = Allocation.createTyped(renderScript, warpInAllocation.getType());
        warpScript = new ScriptC_warp(renderScript);

        warpScript.set_width(bitmap.getWidth());
        warpScript.set_height(bitmap.getHeight());
        warpScript.set_cancelled(false);
        warpScript.bind_input(warpInAllocation);
        warpScript.bind_output(warpOutAllocation);
    }

    public Bitmap getBitmap(){
        return bitmap;
    }

    public void cancelWarp(){
        if(warpScript != null){
            warpScript.set_cancelled(false);
        }
    }

    public abstract Bitmap warp();
}
