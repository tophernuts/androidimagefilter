#pragma version(1)
#pragma rs java_package_name(hey.rich.androidimagefilter)

#define PI 3.141592653589793238462643383279502884197169399375

const uchar4* input;
uchar4* output;

int width, height;

// Someone above us cancelled me
bool cancelled;

static uchar4 getPixelAt(int x, int y){
    if(y>= height){
        y = height -1;
    }else if(y < 0){
        y = 0;
    }

    if(x >= width){
        x = width;
    }else if(x < 0){
        x = 0;
    }

    return input[y*width + x];
}

void setPixelAt(int x, int y, uchar4 pixel){
    if(x > 0 && x < width && y > 0 && y < height){
        output[y*width + x] = pixel;
    }
}

void ripple() {
    float rippleCenterX = width / 2.0f;
    float rippleCenterY = height / 2.0f;
    float radius = min(rippleCenterX, rippleCenterY);
    float radius2 = radius*radius;
    float wavelength = radius / 5.0f;
    // X amplitude
    float amplitude = 5.0f;
    int x, y;
    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            float dx = x - rippleCenterX;
            float dy = y - rippleCenterY;
            float distance2 = dx*dx + dy*dy;
            if (distance2 > radius2) {
                setPixelAt(x,y,getPixelAt(x,y));
            } else {
                float distance = (float)sqrt(distance2);
                float amount = amplitude * (float)cos((float)(distance / wavelength * PI*2.0f));
                amount *= (radius-distance)/radius;
                if ( distance != 0 )
                    amount *= wavelength/distance;
                setPixelAt(x, y, getPixelAt(x + dx*amount, y + dy*amount));
            }

            // Stop warping if cancelled
            if(cancelled){
                return;
            }
        }
    }
}


static float mod(float a, float b) {
    int n = (int)(a/b);

    a -= n*b;
    if (a < 0)
        return a + b;
    return a;
}

static float triangle(float x) {
    float r = mod(x,1.0f);
    if (r < 0.5) {
        r = 2.0f*r;
    } else {
        r = 2.0f*(1-r);
    }
    return r;
}

void kaleidoscope() {

    float midX = width / 2;
    float midY = height / 2;
    float angle = 45;
    float otherAngle = 60;
    float edges = 5;
    float radius = min(midX, midY);

    int x, y;
    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            float dy = y-midY;
            float dx = x-midX;
            float r = sqrt( dx*dx + dy*dy );
            float theta = atan2( dy, dx ) - angle - otherAngle;
            theta = triangle((float)( theta/PI*edges*0.5 ));
            if ( radius != 0 ) {
                float c = cos(theta);
                float radiusc = radius/c;
                r = radiusc * triangle( (float)(r/radiusc) );
            }
            theta += angle;

            setPixelAt(x,y,getPixelAt(midX + r*cos(theta), midY + r*sin(theta)));
        }
    }
}


void fishEye() {

    float midX = width / 2;
    float midY = height / 2;

    float radius = min(midX, midY);
    float radius2 = radius*radius;
    float amount = 1.0f;
    float angle = 0;

    int x, y;
    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            float dx = x-midX;
            float dy = y-midY;
            float distance = dx*dx + dy*dy;

            if ( distance > radius2 || distance == 0 ) {
                setPixelAt(x,y,getPixelAt(x,y));
            } else {
                float d = (float)sqrt( distance / radius2 );
                float t = (float)pow( sin( (float)(PI*0.5 * d) ), amount);

                dx *= t;
                dy *= t;

                float e = 1 - d;
                float a = angle * e * e;

                float s = (float)sin( a );
                float c = (float)cos( a );

                setPixelAt(x,y,getPixelAt(midX + c*dx - s*dy, midY + s*dx + c*dy));
            }
        }
    }
}